package edu.phystech.demochat.rest;

import edu.phystech.demochat.dataasset.dto.MessageDTO;
import edu.phystech.demochat.service.ChatService;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ChatRestController {

    @Autowired
    private ChatService chatService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addMessage(@RequestBody MessageDTO messageDTO)
        throws SQLException {

        return chatService.addMessage(messageDTO);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Map<Integer,List<String>>> getMessages(@RequestParam(defaultValue = "1") int chat){

        return chatService.getMessages(chat);
    }

}
