package edu.phystech.demochat.dataasset.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDTO {

    private String name;
    private String text;
    private String time;

    private int sender;
    private int receiver;


    public MessageDTO() {
    }

    public String getName() {
        return name;
    }

    public MessageDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getText() {
        return text;
    }

    public MessageDTO setText(String text) {
        this.text = text;
        return this;
    }

    public String getTime() {
        return time;
    }

    public MessageDTO setTime(String time) {
        this.time = time;
        return this;
    }
}
